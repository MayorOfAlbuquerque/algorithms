import graph.*;

import java.util.*;

public class Dijkstra {

    private Graph<Integer, Integer> graph;
    public Dijkstra(Graph<Integer, Integer> graphInput) {
        graph = graphInput;
    }

    public List<Integer> shortestPath(Integer origin, Integer destination) {

        List<Integer> list = new ArrayList();


        int[] nodeTable = new int[graph.getNodes().size()+1];
        int[] nearestPrevNodeTable = new int[graph.getNodes().size()+1];
        for(int i = 0; i < graph.getNodes().size()+1; i++) {
            nodeTable[i] = 10000;
        }
        nodeTable[0] = 0;
        nodeTable[1] = 0;
        List<Node<Integer>> visitedNodes = new ArrayList();
        List<Node<Integer>> unVisitedNodes = new ArrayList();
        unVisitedNodes.addAll(graph.getNodes());
        visitedNodes.add(graph.getNode(origin));
        unVisitedNodes.remove(graph.getNode(origin));

        Node<Integer> currentNode = graph.getNode(origin);

        Edge<Integer, Integer> nextShortestEdge;

        while(!unVisitedNodes.isEmpty()) {
            nextShortestEdge = findShortestEdge(visitedNodes, nodeTable);
            distancesFromNode(currentNode, nodeTable, nearestPrevNodeTable);
            currentNode = nextShortestEdge.getTarget();
            visitedNodes.add(currentNode);
            unVisitedNodes.remove(currentNode);

        }
        list.add(destination);
        int graphPointer = destination;
        while(graphPointer != 1) {
            list.add(nearestPrevNodeTable[graphPointer]);
            graphPointer = nearestPrevNodeTable[graphPointer];

        }
        list = reverse(list);


        return list;
    }

    private void distancesFromNode(Node<Integer> currentNode, int[] nodeTable, int[] nearestPrevNodeTable) {

        List<Edge<Integer, Integer>> edges = graph.getEdgesFrom(currentNode);
        for(Edge<Integer, Integer> edge : edges) {
            if(edge.getData() + nodeTable[currentNode.getIndex()] < nodeTable[edge.getTarget().getIndex()]) {
                nearestPrevNodeTable[edge.getTarget().getIndex()] = currentNode.getIndex();
                nodeTable[edge.getTarget().getIndex()] = edge.getData() + nodeTable[currentNode.getIndex()];
            }
        }

    }

    private Edge<Integer, Integer> findShortestEdge(List<Node<Integer>> visitedNodes, int[] nodeTable) {
        Edge<Integer, Integer> shortestEdge = graph.getEdges().get(1);
        int shortestValue = 10000;
        for(Node<Integer> node : visitedNodes) {
            List<Edge<Integer, Integer>> edges = graph.getEdgesFrom(node);
            for(Edge<Integer, Integer> edge : edges) {
                if(edge.getData()+nodeTable[node.getIndex()] < shortestValue && !nodeInNodes(visitedNodes, edge.getTarget())) {
                    shortestEdge = edge;
                    shortestValue = shortestEdge.getData()+nodeTable[node.getIndex()];
                }
            }
        }
        return shortestEdge;
    }

    private boolean nodeInNodes(List<Node<Integer>> nodes, Node givenNode) {
        for(Node node : nodes) {
            if(node.equals(givenNode)) return true;
        }
        return false;
    }


    public List<Integer> reverse(List<Integer> list) {
        if(list.size() > 1) {
            Integer value = list.remove(0);
            reverse(list);
            list.add(value);
        }
        return list;
    }
}
